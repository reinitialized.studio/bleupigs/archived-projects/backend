import "reflect-metadata";
import GenericRouteController from "src/api/webserver/impl/GenericRouteController"
import {IllegalArgumentException} from "src/util/builtIns/Exceptions";
import {FastifyGenerics} from "src/types/api/webserver/impl/FastifyGenerics";
import Method = webServerDecorators.Method;
import cachedRouteController = FastifyGenerics.cachedRouteController;

/**
 * Provides decorators which can annotate class methods of classes that extend {@code GenericRouteController}.
 *
 * @param route: The route of the request. The string most contain a preceding slash but not a
 * trailing slash.
 */

function validateRoute(route: string): boolean {
  const [firstCharacter, lastCharacter] = [route.charAt(0), route.charAt(route.length - 1)];

  if (firstCharacter !== "/"){
    throw new IllegalArgumentException(
      `The preceding character of route - "${route}" must be a forward slash.`
      + ` Got "${firstCharacter}", expected: "/"`
    )
  } else if (lastCharacter === "/"){
    throw new IllegalArgumentException(`The route ${route} should not include the trailing slash`)
  }
  return true
}

function constructorDecorator(
  requestType: Method
){
  return (function(route: string) {
    validateRoute(route);
    return function(target, propertyKey: string, descriptor: PropertyDescriptor) {
      const cachedRoute: cachedRouteController = {
        type: requestType.toLowerCase(),
        path: route,
        handler: target[propertyKey]
      };

      const routeCache = Reflect.getMetadata(GenericRouteController.metadataKey, target);
      if (routeCache){
        routeCache.push(cachedRoute);
        Reflect.defineMetadata(GenericRouteController.metadataKey, routeCache, target);
      } else {
        Reflect.defineMetadata(GenericRouteController.metadataKey, [cachedRoute], target);
      }
    };
  })
}

export const METHODS = {
  get: constructorDecorator("GET"),
  post: constructorDecorator("POST")
};

