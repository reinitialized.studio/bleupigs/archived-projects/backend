import validateUUIDType = utilValidationHelpers.validateUUIDType;

export function validateUUID(UUID: string): validateUUIDType {
  const GenericUUIDLength = 36;
  const UUIDRegex: RegExp = /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89aAbB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}/;
  // TODO: Write unit tests for these. All core logic should have unit tests (even if some HTTP and
  //  GQL database related calls need to be mocked)
  const isRegexValid = UUIDRegex.test(UUID);

  if (UUID && isRegexValid && UUID.length === GenericUUIDLength){
    return {
      valid: true,
      data: {
        RegExp: UUID.match(UUIDRegex),
        length: UUID.length
      }
    }
  }

  return {
    valid: false,
    error: {
      message: `The UUID ${UUID} does not conform to the expected standards. Ensure it matches the `
        + `RegEx ${UUIDRegex} and is of length: ${GenericUUIDLength}. Got length: ${UUID ? UUID.length : "N/A"}`,
    }
  }
}
