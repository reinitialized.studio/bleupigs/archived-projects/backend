import * as implHelpers from "./internal/validationHelpersImpl"
import config from "src/config"
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";

// Provides various UUID related utils.
export const validateUUID = (UUID: string): utilValidationHelpers.validateUUIDType => implHelpers.validateUUID(UUID);
export const validateMultipleUUIDs = (UUIDArray: string[]): utilValidationHelpers.validateMultipleUUIDType => {
  const UUIDMap = UUIDArray.map(UUID => implHelpers.validateUUID(UUID));
  return {
      valid: UUIDMap.every(UUID => UUID.valid === true),
      data: UUIDMap.reduce((accumulator, currentValue) => {
        accumulator.push(currentValue.data ? {
          valid: true,
          data: currentValue.data
        } : {
          valid: false,
          error: currentValue.error.message
        });
        return accumulator
      }, []
    )
  }
};
export const extractInvalidUUIDErrorMessage = (UUIDMap: utilValidationHelpers.validateMultipleUUIDType): utilValidationHelpers.UUIDError[] => {
  if (UUIDMap.valid){
    throw new Error("Cannot extract invalid UUID error messages from a valid UUID map")
  } else {
    return UUIDMap.data.filter(UUID => !UUID.valid).map(UUID => UUID.error)
  }
};

export const isEmpty = (obj) => {
  if (Array.isArray(obj) || typeof obj === "string"){
    return obj.length === 0
  } else if (typeof obj === "object"){
    return Object.entries(obj).length === 0
  } else if (typeof obj === "number"){
    return Number.isNaN(obj)
  } else {
    return !obj
  }
};

export const assert = (condition, error) => {
  if (isEmpty(condition)){
    throw new Error(error)
  }
};

// Very shit implementation of shallow equals. Ensures the duality of two objects where the keys of
// two instances are interchangeable.
export const shallowEquals = (object1: Object, object2: Object, _stack?: Array<boolean>): utilValidationHelpers.shallowEqualsInterface => {
  let stack = _stack || [];
  Object.entries(object1).map(
    ([k, v]) => {
      if (object2.hasOwnProperty(k)) {
        if (Array.isArray(object2[k])){
          stack.push(object2[k].includes(v));
        } else if (typeof v === "object") {
          return shallowEquals(v, object2[k], stack);
        } else {
          stack.push(object2[k] === v)
        }
      }
    }
  );

  return {
    valid: stack.every(item => item === true),
    stack: stack
  };
};

export const isHierarchicallyValidated = (requesterRole: config.validUserRole, requiredRole: config.validUserRole): utilValidationHelpers.isHierarchicallyValidatedInterface => {
  return {
    isMet: config.auth.userTypes[requesterRole] >= config.auth.userTypes[requiredRole],
    requesterRole: requesterRole,
    requiredRole: requiredRole
  }
};

export const requireContext = (userRoleContextData: utilValidationHelpers.isHierarchicallyValidatedInterface) => {
  if (!userRoleContextData.isMet){
    throw new Error(
      `The minimum authorisation role has not been met - current role: ` +
      `${userRoleContextData.requesterRole}, required: ${userRoleContextData.requiredRole}`
    )
  }
};

export const createApolloContext = (
  authenticationRole: config.validUserRole,
  serverContext: ApolloServerLoaderNamespace.serverContext
): ApolloServerLoaderNamespace.Context => {
  const userContext: ApolloServerLoaderNamespace.userContext = {
    role: authenticationRole
  };

  Object.keys(config.auth.userTypes).forEach(
    role =>
      userContext[`is${role[0]}${role.toLowerCase().slice(1, role.length)}OrHigher`] = (() =>
        isHierarchicallyValidated(authenticationRole, <config.validUserRole>role))()
  );

  return {
    serverContext: serverContext,
    userContext: userContext
  };
};
