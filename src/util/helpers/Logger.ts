import chalk from "chalk";

// TODO(Z_V): This is shit.
export default class Logger {
  private readonly tag: string;

  constructor(tag: string) {
    this.tag = tag;
  }

  public log(args){
    return console.log(
      this.formatTag(),
      chalk.greenBright("LOG"),
      Logger.formatTimestamp(),
      args
    )
  }

  public severe(args){
    return console.log(
      this.formatTag(),
      chalk.bgWhiteBright.red.bold.italic.underline("!! SEVERE !!"),
      Logger.formatTimestamp(),
      chalk.bgRedBright.whiteBright.bold.underline(args)
    )
  }

  public error(args){
    return console.log(
      this.formatTag(),
      chalk.redBright("ERROR"),
      Logger.formatTimestamp(),
      args
    )
  }

  public getChalk() {
    return chalk;
  }

  private formatTag(): string {
    return `${chalk.bold(`[${this.tag}]:`)}`
  }

  private static formatTimestamp(){
    return `${chalk.underline(new Date().toLocaleDateString())} - ${chalk.underline(new Date().toLocaleTimeString())}`
  }
}
