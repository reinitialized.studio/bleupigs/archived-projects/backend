import config from "src/config";
import request from "request-promise";

export default class GraphQLClient {
  private query: string;
  private variables: Object;

  public setQuery(query: string): GraphQLClient {
    this.query = query;
    return this;
  }

  public setVariables(variableObj: Object): GraphQLClient {
    this.variables = variableObj;
    return this;
  }

  public async execute() {
    let queryResponse = await request.post({
      uri: config.graphQL.endpoint,
      headers: {
        "content-type": "application/json",
        "x-hasura-admin-secret": config.graphQL.adminSecret
      },
      body: JSON.stringify({
        query: this.query,
        variables: this.variables
      })
    });

    return JSON.parse(queryResponse)
  }
}
