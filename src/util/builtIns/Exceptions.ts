/**
 * Syntactical sugar to allow clients to construct custom Error objects.
 */
class GenericException extends Error {
  __proto__: GenericException;
  public constructor(message?: string) {
    super(message);
    const __proto = new.target.prototype;
    if (Object.setPrototypeOf) { Object.setPrototypeOf(this, __proto); }
    else { this.__proto__ = __proto; }
  }
}

export class IllegalArgumentException extends GenericException {
  public constructor(message?: string) {
    super(`IllegalArgumentException - ${message}`);
  }
}

