import GenericGqlApolloClientController from "src/api/apollo/impl/GenericGqlApolloClientController";
import {GraphQLResolverMap} from "@apollographql/apollo-tools/src/schema/resolverMap";

import {DocumentNode} from "graphql";
import {gql} from "apollo-server";
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";
import {
  extractInvalidUUIDErrorMessage,
  requireContext,
  validateMultipleUUIDs,
  validateUUID
} from "src/util/helpers/validationHelpers";
import config from "src/config";
import ServerValidationDelegate from "src/services/serviceValidation/serverValidationDelegate";
import JwtFactory from "src/services/jwtUtils/jwtFactory";
import Logger from "src/util/helpers/Logger";

export class ValidateRobloxPlaceInstance extends GenericGqlApolloClientController {

  private static GRAPHQL_PERMISSION: config.validUserRole = "ROBLOX";

  private robloxServerValidationDelegate: ServerValidationDelegate;
  private jwtFactory: JwtFactory;
  private logger = new Logger("ValidateRobloxPlaceInstance");

  async getDataSource(requestParameters: validateRobloxPlaceInstanceNamespace.gql$$requestParameters): Promise<validateRobloxPlaceInstanceNamespace.gql$$responseParameters> {
    const routeResponsePromise = await this.validateInstantiateRoute(
      requestParameters.guild, requestParameters.job_id,
      requestParameters.place_id, requestParameters.ip
    );

    if (!routeResponsePromise.success) {
      throw new Error(`Unable to validate game instance. ${routeResponsePromise.response}`);
    }

    let whitelistAddressResponse = await this.robloxServerValidationDelegate.mapWhitelistMachineAddress({
      serverGuild: requestParameters.guild,
      machineAddress: routeResponsePromise.dataLayer.serverCredentialsResponse.MachineAddress,
      serverJobId: requestParameters.job_id
    });

    if (!whitelistAddressResponse.success){
      throw new Error(`Unable map whitelisted server credentials: ${whitelistAddressResponse.response}`);
    }

    return {
      success: true,
      authentication_nodes: {
        graph_ql: this.GQLResponse,
        server_response: whitelistAddressResponse.response.insert_running_servers.returning[0]
      }
    };
  }

  async getDataSourceRefreshHandler(requestParameters: validateRobloxPlaceInstanceNamespace.gql$$refreshRequestParameters): Promise<validateRobloxPlaceInstanceNamespace.gql$$responseParameters> {
    const routeResponsePromise = await this.validateRefreshRoute(
      requestParameters.guild,
      requestParameters.ip
    );

    if (!routeResponsePromise.success) {
      throw new Error(`Unable to validate request. ${routeResponsePromise.response}`);
    }

    return {
      success: true,
      authentication_nodes: {
        graph_ql: this.GQLResponse
      }
    }
  }

  getResolver(): GraphQLResolverMap<any> {
    return {
      Mutation: {
        validateRobloxPlaceInstance: (parent, args: validateRobloxPlaceInstanceNamespace.validateRobloxPlaceInstanceRequestParameters, context: ApolloServerLoaderNamespace.Context) => {
          requireContext(context.userContext.isGuestOrHigher);

          return this.getDataSource({
            ...args.requestData,
            ip: context.serverContext.forwardedFor
          });
        },
        refreshTokenFromRobloxPlace: (parent, args: validateRobloxPlaceInstanceNamespace.refreshRobloxPlaceInstanceRequestParameters, context: ApolloServerLoaderNamespace.Context) => {
          requireContext(context.userContext.isGuestOrHigher);
          // For additional security we can require context isRobloxOrHigher

          return this.getDataSourceRefreshHandler({
            ...args.requestData,
            ip: context.serverContext.forwardedFor
          });
        }
      }
    };
  }

  getSchema(): DocumentNode {
      return gql`        
          input validateRobloxPlaceInstance_requestData {
              place_id: String!
              guild: String!
              job_id: String!
          }

          input validateRobloxPlaceInstance_refreshRequestData {
              guild: String!
          }
          
          type validateRobloxPlaceInstance_responseData {
              success: Boolean
              authentication_nodes: validateRobloxPlaceInstance_authenticationNodes
          }

          type validateRobloxPlaceInstance_refreshResponseData {
              success: Boolean
              authentication_nodes: validateRobloxPlaceInstance_refreshAuthenticationNodes
          }

          extend type Mutation {
              validateRobloxPlaceInstance(requestData: validateRobloxPlaceInstance_requestData!): validateRobloxPlaceInstance_responseData
              refreshTokenFromRobloxPlace(requestData: validateRobloxPlaceInstance_refreshRequestData!): validateRobloxPlaceInstance_refreshResponseData
          }

          type validateRobloxPlaceInstance_graphql {
              alg: String!
              token: String!
              expiry: String!
              permission: String!
          }

          type validateRobloxPlaceInstance_authenticationNodes {
              graph_ql: validateRobloxPlaceInstance_graphql
              server_response: validateRobloxPlaceInstance_serverResponse
          }
          
          type validateRobloxPlaceInstance_refreshAuthenticationNodes {
              graph_ql: validateRobloxPlaceInstance_graphql
          }
          
          type validateRobloxPlaceInstance_serverResponse {
              server_id: Int
              server_job_id: String
              server_guild: String
              server_ip: String
          }
      `;
  }

  moduleSetup(modules?): void {
    this.robloxServerValidationDelegate = new ServerValidationDelegate(config.robloxSettings.join_game_cookie);
    this.jwtFactory = new JwtFactory();
  }

  private get GQLResponse(): validateRobloxPlaceInstanceNamespace.authenticationNodesGraphQLResponse {
    let generatedJwtToken = this.generateJwtToken;
    return {
      alg: JwtFactory.algorithm,
      token: generatedJwtToken.key,
      expiry: generatedJwtToken.expiry,
      permission: ValidateRobloxPlaceInstance.GRAPHQL_PERMISSION
    }
  }

  private get generateJwtToken() {
    return this.jwtFactory.constructClaim(ValidateRobloxPlaceInstance.GRAPHQL_PERMISSION)
  }

  private async validateInstantiateRoute(guild, jobId, placeId, requestIp): Promise<robloxControllerNamespace.validateInstantiateRouteResponse> {
    if (process.env.DEVELOPMENT.toLowerCase() === "true") requestIp = process.env.DEVELOPMENT_MOCK_IP;

    const staticUUIDValidation = validateMultipleUUIDs([guild, jobId]);
    if (!staticUUIDValidation.valid) {
      return {
        success: false,
        response: "Failed static RegEx UUID validation.",
        dataLayer: {
          error: extractInvalidUUIDErrorMessage(staticUUIDValidation)
        }
      }
    }

    if (!config.robloxSettings.validPlaces.includes(placeId)) {
      return {
        success: false,
        response: "Failed static PlaceId validation.",
      }
    }

    const serverCredentialResponsePromise = await this.robloxServerValidationDelegate.requestServerCredentials(jobId, placeId);
    if (!serverCredentialResponsePromise.success) {
      return {
        success: false,
        response: serverCredentialResponsePromise.response,
      }
    }

    if (requestIp !== serverCredentialResponsePromise.data.MachineAddress){
      return {
        success: false,
        response: "Requester IP does not match Roblox server."
      }
    }

    let isInstanceAlreadyValidated = await this.robloxServerValidationDelegate.retrieveWhitelistedServers({
      machineAddress: requestIp,
      serverGuild: guild
    });

    if (isInstanceAlreadyValidated.machineAddress === requestIp){
      if (isInstanceAlreadyValidated.isValid){
        this.logger.severe(
          `Detected attempt to mock server validation workflow. JobId: ${jobId}, guild: ${guild}. ` +
          `The guild is valid - either the Roblox instance requested to initiate twice or the guild has ` +
          `been compromised.`);
        /**
         * TODO: Rotate the env secret keys. Restart the GQL server instance. Alert developers.
         *  Attempt to trigger instance shutdown.
         */
      } else {
        this.logger.log(
          `Detected attempt to mock server validation workflow. JobId: ${jobId}, Guild: ${guild}`
        );
      }
      return {
        success: false,
        response: "Requested instance already validated.",
      }
    }

    return {
      success: true,
      response: serverCredentialResponsePromise.response,
      dataLayer: {
        serverCredentialsResponse: serverCredentialResponsePromise.data
      }
    };
  }

  private async validateRefreshRoute(guild, requestIp) {
    if (process.env.DEVELOPMENT.toLowerCase() === "true") requestIp = process.env.DEVELOPMENT_MOCK_IP;

    const staticUUIDValidation = validateUUID(guild);

    if (!staticUUIDValidation.valid) {
      return {
        success: false,
        response: "Failed static RegEx UUID validation.",
        dataLayer: {
          error: staticUUIDValidation.error
        }
      }
    }

    let isInstanceValid = await this.robloxServerValidationDelegate.retrieveWhitelistedServers({
      machineAddress: requestIp,
      serverGuild: guild
    });

    if (isInstanceValid.machineAddress === requestIp && !isInstanceValid.isValid){
      this.logger.log(`Detected attempt to mock JWT request route. Machine Address: ${requestIp}. Provided guild: ${guild}.`);
    }

    if (!isInstanceValid.isValid) {
      return {
        success: false,
        response: "Requested failed authentication checks - guild or IP does not match the DB.",
      }
    }

    return {
      success: true,
      response: "Server validated."
    }
  }
}
