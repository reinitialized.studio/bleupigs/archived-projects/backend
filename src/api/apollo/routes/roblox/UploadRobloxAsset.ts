import {DocumentNode} from "graphql";
import {GraphQLResolverMap} from "@apollographql/apollo-tools/src/schema/resolverMap";

import GenericGqlApolloClientController from "src/api/apollo/impl/GenericGqlApolloClientController";
import RobloxApiWrapper from "src/loaders/RobloxApiWrapper";
import {gql} from "apollo-server";
import {requireContext} from "src/util/helpers/validationHelpers";
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";

/**
 * Provides a utility to upload assets to Roblox. These assets must conform to the expected type.
 * The mutation requires the following arguments. Note asset_type is an Enum defined in the schema
 * below.
 *
 * mutation {
    uploadRobloxAsset(scriptMetadata: {
      asset_contents: ASSET_CONTENTS,
      asset_name: ASSET_NAME,
      asset_type: ASSET_TYPE
    }) {
      asset_id asset_version_id
    }
  }
 */
export class UploadRobloxAsset extends GenericGqlApolloClientController {
  private robloxApiWrapper: RobloxApiWrapper;

  async getDataSource(scriptMetadata: scriptUploaderNamespace.gql$$scriptUploadRequest): Promise<scriptUploaderNamespace.gql$$responseData> {
    const assetUpload = await this.robloxApiWrapper.uploadInstance(scriptMetadata);

    if (!assetUpload.success){
      throw new Error(`Unable to upload asset. ${assetUpload.error || "N/A"}`)
    }

    return {
      asset_id: assetUpload.response.AssetId,
      asset_version_id: assetUpload.response.AssetVersionId
    };
  }

  getResolver(): GraphQLResolverMap<any> {
    return {
      Mutation: {
        uploadRobloxAsset: (parent, args: robloxControllerNamespace.uploadRobloxAssetRequestParameters, context: ApolloServerLoaderNamespace.Context) => {
          requireContext(context.userContext.isRobloxOrHigher);

          return this.getDataSource(args.requestData);
        }
      },
    };
  }

  getSchema(): DocumentNode {
    return gql`
        enum UploadRobloxAsset_assetTypeEnum {
            SANDBOX
            CLIENT
        }
        
        input UploadRobloxAsset_requestData {
            asset_name: String!
            asset_contents: String!
            asset_type: UploadRobloxAsset_assetTypeEnum!
        }

        type UploadRobloxAsset_responseData {
            asset_id: String
            asset_version_id: String
        }

        type Mutation {
          uploadRobloxAsset(requestData: UploadRobloxAsset_requestData!): UploadRobloxAsset_responseData
        }
    `;
  }

  moduleSetup(modules?): void {
    this.robloxApiWrapper = modules.robloxApiWrapper;
  }
}
