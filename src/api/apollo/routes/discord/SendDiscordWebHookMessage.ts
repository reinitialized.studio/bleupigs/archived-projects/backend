import GenericGqlApolloClientController from "src/api/apollo/impl/GenericGqlApolloClientController";
import {gql} from "apollo-server";
import {DocumentNode} from "graphql";
import {GraphQLResolverMap} from "@apollographql/apollo-tools/src/schema/resolverMap";
import DiscordClient from "src/loaders/DiscordClient";
import Discord, {WebhookClient} from "discord.js";
import config from "src/config";
import {requireContext} from "src/util/helpers/validationHelpers";
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";

/**
 * Allows the Roblox server to send WebHook messages to a predefined channel.
 *
 * mutation {
    sendDiscordWebHookMessage(requestData: {
      user_name: USER_NAME",
      contents: CONTENTS,
      timestamp: TIMESTAMP,
      profile_picture: PROFILE_PICTURE_URL
    }){
      sent
    }
  }
 */
export class SendDiscordWebHookMessage extends GenericGqlApolloClientController {
  private static readonly DISCORD_WEBHOOK_TOKEN = config.discordSettings.crossChatService.webhookToken;
  private static readonly DISCORD_WEBHOOK_ID = config.discordSettings.crossChatService.webhookId;

  private discordClient: DiscordClient;
  private webHookClient: WebhookClient;

  private static webhookToTimestampMap: sendDiscordWebHookMessageNamespace.webhookMap[] = [];

  getDataSource(sent: boolean): sendDiscordWebHookMessageNamespace.gql$$responseData {
    return {
      sent: sent
    };
  }

  getResolver(): GraphQLResolverMap<any> {
    return {
      Mutation: {
        sendDiscordWebHookMessage: async (parent, args: sendDiscordWebHookMessageNamespace.uploadRobloxAssetRequestParameters, context: ApolloServerLoaderNamespace.Context) => {
          requireContext(context.userContext.isRobloxOrHigher);

          const message = await this.webHookClient.send(args.requestData.contents, {
            username: args.requestData.user_name,
            avatarURL: args.requestData.profile_picture,
          });

          SendDiscordWebHookMessage.mutateWebhookMap(message.id, args.requestData.timestamp);
          return this.getDataSource(true);
        }
      }
    };
  }

  getSchema(): DocumentNode {
      return gql`        
          input sendDiscordWebHookMessage_requestData {
              user_name: String!
              contents: String!
              profile_picture: String!
              timestamp: Int!
          }

          type sendDiscordWebHookMessage_responseData {
              sent: Boolean
          }

          extend type Mutation {
              sendDiscordWebHookMessage(requestData: sendDiscordWebHookMessage_requestData!): sendDiscordWebHookMessage_responseData
          }
      `;
  }

  private static mutateWebhookMap(webhookId: string, timestamp: number){
    SendDiscordWebHookMessage.webhookToTimestampMap.push({
      id: webhookId,
      timestamp: timestamp
    })
  }

  public static retrieveTimestampByWebhookId(id: string){
    let webhookTimestamp: sendDiscordWebHookMessageNamespace.webhookMap[] = SendDiscordWebHookMessage.webhookToTimestampMap.filter(
      webhookMap => webhookMap.id === id
    );

    if (webhookTimestamp.length === 0){
      return null;
    }

    return webhookTimestamp[0].timestamp
  }

  moduleSetup(modules?): void {
    this.discordClient = modules.discordClient;
    this.webHookClient = new Discord.WebhookClient(SendDiscordWebHookMessage.DISCORD_WEBHOOK_ID, SendDiscordWebHookMessage.DISCORD_WEBHOOK_TOKEN);
  }

}
