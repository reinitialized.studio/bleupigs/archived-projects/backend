import {gql} from "apollo-server";
import GenericGqlApolloClientController from "src/api/apollo/impl/GenericGqlApolloClientController";
import DiscordClient from "src/loaders/DiscordClient";
import {Message} from "discord.js";
import config from "src/config";
import {SendDiscordWebHookMessage} from "src/api/apollo/routes/discord/SendDiscordWebHookMessage";
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";
import {requireContext} from "src/util/helpers/validationHelpers";

/**
 * Provides a utility to get the recent messages from a predefined channel. The GQL query expects
 * a Unix timestamp to be passed as an argument.
 *
 * query {
    recentCachedDiscordMessages (since: UNIX_TIMESTAMP){
      number_of_messages collected_at
      messages {
        author content nickname timestamp
        display_hex_color hoist_or_highest_role hoist_or_highest_role_hex_color
      }
    }
  }
 */
export class GetRecentCachedDiscordMessages extends GenericGqlApolloClientController {
  private static readonly DISCORD_CHAT_CHANNEL_ID = config.discordSettings.crossChatService.discordChannelId;
  private static readonly MAXIMUM_MESSAGE_CACHE = 200;

  private discordClient: DiscordClient;
  private messageQueue: discordApiRoutesNamespace.messageQueue[] = [];

  public getSchema(){
    return gql`
        type recentCachedDiscordMessages_Message {
            author: String,
            content: String,
            nickname: String,
            timestamp: Int,
            display_hex_color: String,
            hoist_or_highest_role: String,
            hoist_or_highest_role_hex_color: String
            is_webhook: Boolean
            id: String
        }
        
        type recentCachedDiscordMessages_MessageInfo {
            collected_at: Int
            number_of_messages: Int
            messages: [recentCachedDiscordMessages_Message]
        }

        type Query {
            recentCachedDiscordMessages(since: Int!): recentCachedDiscordMessages_MessageInfo
        }
    `;
  }

  public getDataSource(sinceTimestamp: Number){
    return {
      collected_at: sinceTimestamp,
      number_of_messages: this.messageQueue.length,
      messages: this.messageQueue.filter(
        messageData => {
          if (messageData.is_webhook){
            const timeStamp = SendDiscordWebHookMessage.retrieveTimestampByWebhookId(messageData.id);
            // The timestamp is nullable as not every webhook will have been mapped.

            return timeStamp ? timeStamp >= sinceTimestamp : messageData.timestamp >= sinceTimestamp;
          }

          return messageData.timestamp >= sinceTimestamp
        }
      ).map(
        messageData => {
          if (messageData.is_webhook){
            const timeStamp = SendDiscordWebHookMessage.retrieveTimestampByWebhookId(messageData.id);
            if (timeStamp){
              messageData.timestamp = timeStamp
            }
            messageData.author = `~ ${messageData.author}`;
            messageData.display_hex_color = "#7289DA"; // Discord's "Blurple" colour..
          }

          return messageData;
        }
      )
    };
  }

  public getResolver(){
    return {
      Query: {
        recentCachedDiscordMessages: (parent, args, context: ApolloServerLoaderNamespace.Context) => {
          requireContext(context.userContext.isMemberOrHigher);
          const { since } = args;

          if (since > ~~(Date.now()/1000)){
            throw new Error("Cannot request a date in the future.");
          }

          return this.getDataSource(since)
        },
      },
    };
  }

  public moduleSetup(modules?): void {
    this.discordClient = modules.discordClient;
    this.initiateMessageListener();
  }

  private initiateMessageListener(){
    this.discordClient.bindEvent(
      "message",
      {
        channel: {
          id: GetRecentCachedDiscordMessages.DISCORD_CHAT_CHANNEL_ID
        }
      },
      (message: Message) => {
        const immutableMessage: discordApiRoutesNamespace.messageQueue = !message.webhookID ? {
          author: message.author.username,
          nickname:  message.member.displayName,
          content: message.content,
          display_hex_color: message.member.displayHexColor,
          hoist_or_highest_role_name: message.member.roles?.hoist?.name || message.member.roles?.highest?.name || null,
          hoist_or_highest_role_hex_color: message.member.roles?.hoist?.hexColor || message.member.roles?.hoist?.hexColor || null,
          timestamp: ~~(message.createdTimestamp/1000),
          id: message.id,
          is_webhook: false,
        } : {
          author: message.author.username,
          content: message.content,
          is_webhook: true,
          id: message.id,
          timestamp: ~~(message.createdTimestamp/1000),
        };

        if (this.messageQueue.length >= GetRecentCachedDiscordMessages.MAXIMUM_MESSAGE_CACHE){
          this.messageQueue.shift();
        }

        this.messageQueue.push(immutableMessage);
      }
    )
  }
}
