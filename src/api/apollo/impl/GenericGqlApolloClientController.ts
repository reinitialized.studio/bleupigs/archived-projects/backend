import {DocumentNode} from "graphql";
import {GraphQLResolverMap} from "@apollographql/apollo-tools/src/schema/resolverMap";

/**
 * Generic class to provide a structure for Apollo Server GQL clients.
 */
export default abstract class GenericGqlApolloClientController {
  abstract getSchema(): DocumentNode

  abstract getDataSource(...any): Array<any> | Object

  abstract getResolver(): GraphQLResolverMap<any>

  abstract moduleSetup(modules?): void;
}
