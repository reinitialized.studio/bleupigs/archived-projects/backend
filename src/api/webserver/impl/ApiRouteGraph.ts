import {ApiRouteGraphNamespace} from "src/types/api/webserver/impl/ApiRouteGraphNamespace";
import GenericRouteController from "src/api/webserver/impl/GenericRouteController";
import {RouteShorthandOptions} from "fastify";
import chalk from "chalk";
import Logger from "src/util/helpers/Logger";
import fastifyInstance = ApiRouteGraphNamespace.fastifyInstance;
import ApiRouteGraphOptions = ApiRouteGraphNamespace.ApiRouteGraphOptions;

/**
 * Binds the Fastify routes to the Fastify instance with the associated method.
 */
export default class ApiRouteGraph {
  public readonly instance: fastifyInstance;

  private static routeInstanceCache: GenericRouteController[] = [];

  private logger = new Logger("ApiRouteGraph");
  private options: RouteShorthandOptions;

  constructor(options: ApiRouteGraphOptions) {
    this.instance = options.instance
  }

  public setOptions(options: RouteShorthandOptions): ApiRouteGraph {
    this.options = options;
    return this;
  }

  public addAllRoutes(routeClasses): ApiRouteGraph {
    routeClasses.forEach(
      routeInstance => {
        const initiatedInstance: GenericRouteController =
          new routeInstance(this.instance, this.options);
        this.logger.log(`Initiating route: ${chalk.blueBright(routeInstance.name)}`);
        ApiRouteGraph.routeInstanceCache.push(initiatedInstance)
      }
    );
    return this
  }

  public create(modules): void {
    ApiRouteGraph.routeInstanceCache.forEach(
      routeBinding => {
        routeBinding.moduleSetup(modules);
        routeBinding.getMetadata().map(
          routeMap => {
            this.logger.log(
              `Registering ${chalk.redBright(routeMap.type.toLocaleUpperCase())} handler for path: ${chalk.blueBright(routeMap.path)}`
            );

            this.instance[routeMap.type](
              routeMap.path,
              routeMap.handler.bind(routeBinding)
              // We have to bind the function as otherwise references to the class would be lost.
            )
          }
        )
      }
    )
  }
};
