import {FastifyGenerics} from "src/types/api/webserver/impl/FastifyGenerics";
import cachedRouteController = FastifyGenerics.cachedRouteController;

/**
 * Generic class to provide a structure for route classes. Includes helper methods to handle
 * decorator reflection at runtime.
 */
export default abstract class GenericRouteController {
  public static metadataKey = "impl$$routeCache";
  public readonly fastify;
  public options;

  protected constructor(fastify, options?) {
    this.fastify = fastify;
    this.options = options;
  }

  public getMetadata(): cachedRouteController[] {
    return Reflect.getMetadata(GenericRouteController.metadataKey, this) || [];
  }

  abstract moduleSetup(modules?): void;

  public clearMetadata(){
    return Reflect.defineMetadata(GenericRouteController.metadataKey, [], this);
  }

  public deleteMetadata(){
    return Reflect.deleteMetadata(GenericRouteController.metadataKey, this)
  }

  public getMetadataKeys(){
    return Reflect.getMetadataKeys(this)
  }
}
