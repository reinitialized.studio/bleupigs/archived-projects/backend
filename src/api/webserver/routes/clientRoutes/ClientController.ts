import GenericRouteController from "src/api/webserver/impl/GenericRouteController";
import JwtFactory from "src/services/jwtUtils/jwtFactory";
import {METHODS as app} from "src/util/decorators/webServerDecorator";
import {FastifyGenerics} from "src/types/api/webserver/impl/FastifyGenerics";
import DiscordOauth2 from "discord-oauth2"
import config from "src/config"
import crypto from "crypto"

/**
 * Handles Discord OAuth authentication for clients. Provides routes for generating Discord's
 * oauth2 urls and the call back.
 */
export default class ClientController extends GenericRouteController {
  private jwtFactory: JwtFactory;
  private oauthClient: DiscordOauth2;

  @app.post(config.api.routes.discord.DISCORD_OAUTH)
  public async authenticateDiscordUser(req: FastifyGenerics.request, res: FastifyGenerics.response) {
    const oauthUrl = this.oauthClient.generateAuthUrl({
      scope: ["identify"],
      state: crypto.randomBytes(16).toString("hex"),
    });

    return {
      success: true,
      oauthUrl: oauthUrl
    }
  }

  @app.post(config.api.routes.discord.DISCORD_OAUTH_CALLBACK)
  public async discordCallback(req: FastifyGenerics.request, res: FastifyGenerics.response) {
    // Callback from Discord oauth.
  }

  moduleSetup(): void {
    this.jwtFactory = new JwtFactory();
    this.oauthClient = new DiscordOauth2({
      clientId: config.discordSettings.clientId,
      clientSecret: config.discordSettings.clientSecret,
      redirectUri: config.api.hostname + config.api.routes.discord.DISCORD_OAUTH_CALLBACK,
    });
  }
}
