import GenericRouteController from "src/api/webserver/impl/GenericRouteController";
import {METHODS as app} from "src/util/decorators/webServerDecorator";
import {FastifyGenerics} from "src/types/api/webserver/impl/FastifyGenerics";
import ServerValidationDelegate from "src/services/serviceValidation/serverValidationDelegate";
import * as validationHelpers from "src/util/helpers/validationHelpers";
import config from "src/config";
import JwtFactory from "src/services/jwtUtils/jwtFactory";
import Logger from "src/util/helpers/Logger";

/**
 * Handles Roblox authentication and refreshing of JWT tokens for GraphQL requests.
 *
 * @Deprecated in favour of {@link ValidateRobloxPlaceInstance}.
 */
export default class robloxController extends GenericRouteController {
  private static GRAPHQL_PERMISSION: config.validUserRole = "ROBLOX";

  private robloxServerValidationDelegate: ServerValidationDelegate;
  private jwtFactory: JwtFactory;
  private logger = new Logger("RobloxController");

  /**
   * Expected body contents (application/json)
   * {
   *   guild: UUID - Generated guild which persists for a Roblox instances duration.
   *   jobId: UUID
   * }
   * Headers {
   *   Roblox-Id: string (placeId)
   * }
   */
  @app.post(config.api.routes.roblox.VALIDATE_GAME_INSTANCE)
  public async validateHandler(req: FastifyGenerics.request) {
    const {guild, jobId}: { guild: string, jobId: string } = req.body;
    const placeId: string = req.headers["roblox-id"];
    const routeResponsePromise = await this.validateInstantiateRoute(guild, jobId, placeId, req.ip);

    if (!routeResponsePromise.success) {
      return {
        status: "error",
        response: `Unable to validate game instance. ${routeResponsePromise.response}`,
        data: routeResponsePromise.dataLayer?.error
      }
    }

    let whitelistAddressResponse = await this.robloxServerValidationDelegate.mapWhitelistMachineAddress({
      serverGuild: guild,
      machineAddress: routeResponsePromise.dataLayer.serverCredentialsResponse.MachineAddress,
      serverJobId: jobId
    });

    if (!whitelistAddressResponse.success){
      return {
        status: "failure",
        response: "Unable map whitelisted server credentials."
      }
    }

    return {
      status: "success",
      authenticationNodes: {
        graphQL: this.GQLResponse,
        graphQLResponse: whitelistAddressResponse.response.insert_running_servers.returning
      }
    }
  }

  // TODO(Z_V): Pass a class which extends GenericRoutePreconditions for request validation.
  /**
   * Expected body contents (application/json)
   * {
   *   guild: UUID - Generated guild which persists for a Roblox instances duration.
   * }
   */
  @app.post(config.api.routes.roblox.REFRESH_GRAPH_QL_TOKEN)
  public async refreshHandler(req: FastifyGenerics.request) {
    const routeResponsePromise = await this.validateRefreshRoute(req.body.guild, req.ip);

    if (!routeResponsePromise.success) {
      return {
        status: "error",
        response: `Unable to validate request. ${routeResponsePromise.response}`,
        data: routeResponsePromise.dataLayer?.error
      }
    }

    return {
      status: "success",
      authenticationNodes: {
        graphQL: this.GQLResponse
      }
    }
  }

  public async moduleSetup() {
    this.robloxServerValidationDelegate = new ServerValidationDelegate(config.robloxSettings.join_game_cookie);
    this.jwtFactory = new JwtFactory();
  }

  private get GQLResponse(){
    let generatedJwtToken = this.generateJwtToken;
    return {
      alg: JwtFactory.algorithm,
      token: generatedJwtToken.key,
      refresh_link: config.api.hostname + config.api.routes.roblox.REFRESH_GRAPH_QL_TOKEN,
      endpoint: config.graphQL.endpoint,
      expiry: generatedJwtToken.expiry,
      permission: robloxController.GRAPHQL_PERMISSION
    }
  }

  private get generateJwtToken() {
    return this.jwtFactory.constructClaim(robloxController.GRAPHQL_PERMISSION)
  }

  private async validateInstantiateRoute(guild, jobId, placeId, requestIp): Promise<robloxControllerNamespace.validateInstantiateRouteResponse> {
    if (process.env.DEVELOPMENT.toLowerCase() === "true") requestIp = process.env.DEVELOPMENT_MOCK_IP;

    const staticUUIDValidation = validationHelpers.validateMultipleUUIDs([guild, jobId]);
    if (!staticUUIDValidation.valid) {
      return {
        success: false,
        response: "Failed static RegEx UUID validation.",
        dataLayer: {
          error: validationHelpers.extractInvalidUUIDErrorMessage(staticUUIDValidation)
        }
      }
    }

    if (!config.robloxSettings.validPlaces.includes(placeId)) {
      return {
        success: false,
        response: "Failed static PlaceId validation.",
      }
    }

    const serverCredentialResponsePromise = await this.robloxServerValidationDelegate.requestServerCredentials(jobId, placeId);
    if (!serverCredentialResponsePromise.success) {
      return {
        success: false,
        response: serverCredentialResponsePromise.response,
      }
    }

    if (requestIp !== serverCredentialResponsePromise.data.MachineAddress){
      return {
        success: false,
        response: "Requester IP does not match Roblox server."
      }
    }

    let isInstanceAlreadyValidated = await this.robloxServerValidationDelegate.retrieveWhitelistedServers({
      machineAddress: requestIp,
      serverGuild: guild
    });

    if (isInstanceAlreadyValidated.machineAddress === requestIp){
      if (isInstanceAlreadyValidated.isValid){
        this.logger.severe(
          `Detected attempt to mock server validation workflow. JobId: ${jobId}, guild: ${guild}. ` +
          `The guild is valid - either the Roblox instance requested to initiate twice or the guild has ` +
          `been compromised.`
        )
      } else {
        this.logger.log(
          `Detected attempt to mock server validation workflow. JobId: ${jobId}, Guild: ${guild}`
        );
      }
      return {
        success: false,
        response: "Requested instance already validated.",
      }
    }

    return {
      success: true,
      response: serverCredentialResponsePromise.response,
      dataLayer: {
        serverCredentialsResponse: serverCredentialResponsePromise.data
      }
    };
  }

  private async validateRefreshRoute(guild, requestIp) {
    if (process.env.DEVELOPMENT.toLowerCase() === "true") requestIp = process.env.DEVELOPMENT_MOCK_IP;

    const staticUUIDValidation = validationHelpers.validateUUID(guild);

    if (!staticUUIDValidation.valid) {
      return {
        success: false,
        response: "Failed static RegEx UUID validation.",
        dataLayer: {
          error: staticUUIDValidation.error
        }
      }
    }

    let isInstanceValid = await this.robloxServerValidationDelegate.retrieveWhitelistedServers({
      machineAddress: requestIp,
      serverGuild: guild
    });

    if (isInstanceValid.machineAddress === requestIp && !isInstanceValid.isValid){
      this.logger.log(`Detected attempt to mock JWT request route. Machine Address: ${requestIp}. Provided guild: ${guild}.`);
    }

    if (!isInstanceValid.isValid) {
      return {
        success: false,
        response: "Requested failed authentication checks - guild or IP does not match the DB.",
      }
    }

    return {
      success: true,
      response: "Server validated."
    }
  }
}
