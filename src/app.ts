import WebServerLoader from "src/loaders/WebServerLoader"
import RobloxApiWrapper from "src/loaders/RobloxApiWrapper"
import Logger from "src/util/helpers/Logger";
import DiscordClient from "src/loaders/DiscordClient";
import ApolloServerLoader from "src/loaders/ApolloServerLoader";

const webServer = new WebServerLoader(3000);
const apolloServer = new ApolloServerLoader();
const robloxApiWrapper = new RobloxApiWrapper();
const discordClient = new DiscordClient();
const logger = new Logger("app");

const sharedModules = {
  robloxApiWrapper: robloxApiWrapper,
  discordClient: discordClient
};

webServer
  .bindModules(sharedModules)
  .bindRoutes()
  .startWebServer().then(
    response => logger.log(response)
  ).catch(error => {
    logger.error(`Unable to initiate web server: ${error}. Terminating process`);
    process.exit(1)
  });

// robloxApiWrapper
//   .login().then(
//     response => {
//       logger.log(
//         `Logged into Roblox account as ${logger.getChalk().blueBright(response.UserName)} (${logger.getChalk().blueBright(response.UserID)})`
//       )
//     }
//   ).catch(error => {
//     logger.error(`Unable to login to Roblox account: ${error} Terminating process`);
//     process.exit(1)
//   });

discordClient
  .authenticate().then(
    () => logger.log(`Authenticated with ${logger.getChalk().blueBright("Discord")}`)
).catch(error => {
  logger.error(`Unable to authenticate with Discord: ${error}. Terminating process`);
  process.exit(1)
});

apolloServer
  .bindModules(sharedModules)
  .bindRoutes()
  .startApolloServer().then(({ url }) => {
    logger.log(`ApolloServer started and ready at: ${url}`)
  }).catch(error => {
    logger.error(`Unable to start ApolloServer: ${error}. Terminating process`);
    process.exit(1)
  });
