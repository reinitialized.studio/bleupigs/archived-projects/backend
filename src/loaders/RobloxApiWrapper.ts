import config from "src/config";
import noblox from "noblox.js"
import RobloxAssetHelper, {AssetStore} from "src/services/assetServices/RobloxAssetHelper";

/**
 * Provides a wrapper for Noblox.
 */
export default class RobloxApiWrapper {
  public static cookie = config.robloxSettings.upload_asset_cookie;

  private hasAttemptedToLogin: boolean = false;
  private robloxAssetHelper: RobloxAssetHelper;

  constructor() {
    this.robloxAssetHelper = new RobloxAssetHelper(noblox)
  }

  public async login(){
    this.hasAttemptedToLogin = true;
    // @ts-ignore. I've submitted a patch to add this method to the typedefs.
    return await noblox.setCookie(RobloxApiWrapper.cookie)
  }

  public async uploadInstance(scriptMetadata: scriptUploaderNamespace.gql$$scriptUploadRequest): Promise<RobloxApiWrapperNamespace.uploadInstanceResponse> {
    if (!this.hasAttemptedToLogin){
      return {
        success: false,
        error: "The login method of RobloxApiWrapper has not been called!"
      }
    }

    //  TODO(Z_V): This will grow. Consider rewriting so that the cases are encapsulated within
    //    a dict. This will simplify the process of adding new asset types. Ideally the dict will
    //    be abstracted out of this method.
    switch (scriptMetadata.asset_type.toString()){
      case "SANDBOX":
        return {
          success: true,
          response: await this.uploadModuleSandboxedInstance(scriptMetadata)
        };
      default:
        return {
          success: false,
          error: `Enum "${scriptMetadata.asset_type}" is not valid.`
        }
    }
  }

  private async uploadModuleSandboxedInstance(scriptMetadata: scriptUploaderNamespace.gql$$scriptUploadRequest){
    return await this.robloxAssetHelper
      .setType(AssetStore.SANDBOX)
      .setInstanceContents(<sandboxInterface.propertyMap>{
        contents: scriptMetadata.asset_contents
      }).upload(scriptMetadata.asset_name);
  }
}
