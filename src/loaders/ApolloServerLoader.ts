import {ApolloServer} from "apollo-server";
import config from "src/config";
import GenericGqlApolloClientController from "src/api/apollo/impl/GenericGqlApolloClientController";
import {ApolloServerLoaderNamespace} from "src/types/loaders/ApolloServerLoaderNamespace";
import Logger from "src/util/helpers/Logger";
import {GetRecentCachedDiscordMessages} from "src/api/apollo/routes/discord/GetRecentCachedDiscordMessages";
import {UploadRobloxAsset} from "src/api/apollo/routes/roblox/UploadRobloxAsset";
import {SendDiscordWebHookMessage} from "src/api/apollo/routes/discord/SendDiscordWebHookMessage";
import {createApolloContext} from "src/util/helpers/validationHelpers";
import {ValidateRobloxPlaceInstance} from "src/api/apollo/routes/roblox/ValidateRobloxPlaceInstance";

/**
 * Provides a wrapper for the Apollo Server.
 * - Allows the client (entry point) to pass in various GQL schema modules that conform to the
 *   GenericGqlApolloClientController class.
 */
export default class ApolloServerLoader {
  private modules = [];
  private apolloServer: ApolloServer;
  private hasBoundRoutes: boolean = false;
  private logger = new Logger("ApolloServerLoader");

  private readonly apolloServerModulesImpl: ApolloServerLoaderNamespace.GraphQLSchemaModule[] = [];

  public bindModules(modules): ApolloServerLoader {
    this.modules = modules;
    return this;
  }

  public bindRoutes(): ApolloServerLoader {
    this.hasBoundRoutes = true;
    [
      GetRecentCachedDiscordMessages,
      UploadRobloxAsset,
      SendDiscordWebHookMessage,
      ValidateRobloxPlaceInstance
    ].forEach(
      moduleImpl => {
        const moduleInstance: GenericGqlApolloClientController = new moduleImpl();
        this.logger.log(`Initiating GraphQL endpoint for: ${this.logger.getChalk().blueBright(moduleImpl.name)}`);
        moduleInstance.moduleSetup(this.modules);
        this.apolloServerModulesImpl.push({
          typeDefs: moduleInstance.getSchema(),
          resolvers: moduleInstance.getResolver()
        })
      }
    );

    this.apolloServer = new ApolloServer({
      modules: this.apolloServerModulesImpl,
      introspection: true,
      playground: true,
      debug: false,
      tracing: true,
      context: ({req}) => {

        const userTypes = Object.keys(config.auth.userTypes);
        const apolloSecret = req.headers["apollo_secret"];
        const serverContext: ApolloServerLoaderNamespace.serverContext = {
          forwardedHost: req.headers["x-forwarded-host"],
          forwardedFor: req.headers["x-forwarded-for"]
        };

        let authenticationRole = <config.validUserRole>req.headers["x-hasura-role"];

        if (!apolloSecret || apolloSecret !== config.apolloConfig.apolloSecret){
          return createApolloContext(<config.validUserRole>"GUEST", serverContext)
        }

        if (!authenticationRole || !userTypes.includes(authenticationRole)){
          /**
           * This is far from ideal. However Hasura makes an initial request on to  set the remote
           * schema metadata. We must make the assumption that if the APOLLO_SECRET is known but no
           * role is provided then the request is originating from the server.
           * TODO: A better solution would be to keep this authentication logic here and allow an
           *  optional context and move the authorisation layer to a lifecycle event which is
           *  called before each request. The expectation is that event wouldn't be called on
           *  metadata initiation.
           */
          authenticationRole = "SERVER"
        }

        /** TODO: Pull the role out of the JWT. Followed up by a security evaluation. */
        return createApolloContext(authenticationRole, serverContext);
      }
    });

    return this;
  }

  public async startApolloServer(){
    return this.apolloServer.listen()
  }
}
