import fastify, {FastifyInstance} from 'fastify'
import ApiRouteGraph from "src/api/webserver/impl/ApiRouteGraph";
import ClientController from "src/api/webserver/routes/clientRoutes/ClientController";

/**
 * Provides a wrapper for the Fastify instance.
 * - Allows the client (entry point) to pass initialised classes the routes.
 * - Adds the requested routes. These routes must extend {@code GenericRouteController}
 */
export default class WebServerLoader {
  public port: number;
  public readonly server: FastifyInstance;

  private modules;
  private hasBoundRoutes: boolean = false;

  constructor(port: number) {
    this.port = port;
    this.server = fastify()
  }

  public bindModules(modules): WebServerLoader {
    if (this.hasBoundRoutes){
      throw new Error("The modules must be bound before the routes.");
    }
    this.modules = modules;
    return this;
  }

  public bindRoutes(): WebServerLoader {
    this.hasBoundRoutes = true;
    new ApiRouteGraph({instance: this.server})
      .setOptions({
        logSerializers: true
      })
      .addAllRoutes([
        ClientController
      ])
      .create(this.modules);

    return this;
  }

  public async startWebServer(): Promise<string> {
    return new Promise<string>((resolve, reject) =>
      this.server.listen(this.port, "0.0.0.0", (err, address) => {
        if (err) {
          reject(`Web Server unable to start - ${err}`)
        }
        resolve(`Web Server started. Address: ${address}`);
      })
    );
  }
}
