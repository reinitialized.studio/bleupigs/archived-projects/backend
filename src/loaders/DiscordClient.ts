import config from "src/config";
import Discord from "discord.js"
import {shallowEquals} from "src/util/helpers/validationHelpers";
import {DiscordClientNamespace} from "src/types/loaders/DiscordClientNamespace";

export default class DiscordClient implements DiscordClientNamespace.DiscordClientInterface {
  private token: string = config.discordSettings.discordToken;
  private discordClient = new Discord.Client();
  private store = {
    events: {}
  };

  public bindEvent(event, metadata, listener): DiscordClient {
    if (!this.store.events[event]) this.store.events[event] = [];
    metadata.$$func = listener;
    this.store.events[event].push(metadata);
    return this;
  }

  public async authenticate(){
    await this.discordClient.login(this.token);
    this.messageListener();
  }

  // This will ideally be abstracted away. The foundations are in place.
  private async messageListener(){
    this.discordClient.on("message", msg => {
      const event = "message";
      this.store.events[event].forEach(trigger => {
        if (shallowEquals(trigger, msg).valid) trigger["$$func"](msg)
      })
    })
  }
}
