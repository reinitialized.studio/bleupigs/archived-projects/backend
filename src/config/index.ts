const apiConfig: config.apiConfigInternal = {
  hostname: "http://209.97.184.37:8080"
};

export default <config.defaultConfig>{
  jwtConfig: {
    algorithm: "HS256",
    secretKey: process.env.JWT_SECRET_KEY,
    // https://cloud.google.com/vpn/docs/how-to/generating-pre-shared-key
    // is useful for generating pre-shared keys.
    defaultExpiry: "30m",
    expiryOverride: {
      GUEST: "1h",
      ROBLOX: "15m"
    }
  },
  api: {
    ...apiConfig,
    internal: {
      secret: process.env.INTERNAL_AUTHORISATION_SECRET
    },
    routes: {
      roblox: {
        VALIDATE_GAME_INSTANCE: "/auth/roblox/instantiate",
        REFRESH_GRAPH_QL_TOKEN: "/auth/roblox/refresh",
        UPLOAD_SANDBOXED_MODULE: "/internal/roblox/upload_sandboxed_module"
      },
      discord: {
        DISCORD_OAUTH: "/auth/client/discord_auth",
        DISCORD_OAUTH_CALLBACK: "/auth/client/discord_auth_callback"
      },
      discordApi: {
        GET_RECENT_CACHED_MESSAGES: "/internal/discord/get_recent_messages"
      }
    }
  },
  graphQL: {
    endpoint: `${apiConfig.hostname}/v1/graphql`,
    adminSecret: process.env.HASURA_ADMIN_SECRET,
  },
  auth: {
    userTypes: {
      GUEST: 0,
      APPLICANT: 1,
      MEMBER: 2,
      ELITE: 3,
      RESOLVER: 4,
      ARBITRATOR: 5,
      ROBLOX: 6,
      SERVER: 7,
    }
  },
  robloxSettings: {
    validPlaces: ["4734966191", "3915256093", "5079190803", "4528018876"],
    upload_asset_cookie: process.env.UPLOAD_ASSET_ROBLOX_COOKIE,
    join_game_cookie: process.env.JOIN_GAME_ROBLOX_COOKIE
  },
  discordSettings: {
    clientId: "638147302774669312",
    clientSecret: process.env.DISCORD_SECRET_KEY,
    discordToken: process.env.DISCORD_TOKEN,
    crossChatService: {
      discordChannelId: "716606593512243231",
      webhookId: "718879718681739288",
      webhookToken: process.env.DISCORD_WEBHOOK_TOKEN,
    }
  },
  apolloConfig: {
    apolloSecret: process.env.APOLLO_SECRET
  }
};
