import JwtAlgorithm = jwtFactory.Algorithm;
import JwtExpiry = jwtFactory.Expiry;

declare module config {
  // Top level config
  export interface defaultConfig {
    jwtConfig: jwtConfig;
    auth: authConfig;
    robloxSettings: robloxConfig;
    graphQL: graphQLInterface;
    discordSettings: discordSettings;
    api: apiConfig;
    apolloConfig: apolloConfig
  }

  // Config interfaces
  export interface jwtConfig {
    algorithm: JwtAlgorithm;
    secretKey: string | Buffer;
    defaultExpiry: JwtExpiry;
    expiryOverride?: {
      [key: string]: JwtExpiry
    }
  }

  export interface graphQLInterface {
    endpoint: string;
    adminSecret: string
  }

  export interface robloxConfig {
    validPlaces: string[];
    upload_asset_cookie: string;
    join_game_cookie: string
  }

  export interface authConfig {
    userTypes: {
      [user in validUserRole]: number
    }
  }

  export interface discordSettings {
    clientId: string;
    clientSecret: string;
    discordToken: string;
    crossChatService: {
      discordChannelId: string,
      webhookId: string,
      webhookToken: string,
    }
  }

  export interface apiConfig {
    hostname: string,
    internal: {
      secret: string
    }
    routes: {
      roblox: {
        VALIDATE_GAME_INSTANCE: string,
        REFRESH_GRAPH_QL_TOKEN: string,
        UPLOAD_SANDBOXED_MODULE: string
      },
      discord: {
        DISCORD_OAUTH: string,
        DISCORD_OAUTH_CALLBACK: string
      },
      discordApi: {
        GET_RECENT_CACHED_MESSAGES: string
      }
    }
  }

  export interface apolloConfig {
    apolloSecret: string
  }

  interface apiConfigInternal {
    hostname: string,
  }

  // Types
  export type validUserRole = "GUEST"  | "APPLICANT"     | "MEMBER"     |
                              "ELITE"  | "RESOLVER"      | "ARBITRATOR" |
                              "ROBLOX" | "SERVER"

  /**
   * TODO: Move to an enum based approach
     export enum validUserRole {
       GUEST,
       APPLICANT,
       MEMBER,
       ELITE,
       RESOLVER,
       ARBITRATOR,
       ROBLOX,
       SERVER
     }
   */
}
