declare module genericRouteResponses {
  export interface internalRoutesValidationResponse {
    success: boolean,
    response: string,
  }
}
