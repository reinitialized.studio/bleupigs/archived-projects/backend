import {DefaultHeaders, DefaultParams, DefaultQuery, FastifyReply, FastifyRequest} from "fastify";
import * as http from "http";

declare namespace FastifyGenerics {
  export type request =
    FastifyRequest<http.IncomingMessage, DefaultQuery, DefaultParams, DefaultHeaders, any>
  export type response = FastifyReply<http.ServerResponse>

  export type cachedRouteController = {
    type: string,
    path: string,
    handler: Function
  }
}
