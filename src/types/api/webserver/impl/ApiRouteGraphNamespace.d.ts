import fastify from "fastify";
import * as http from "http";

declare namespace ApiRouteGraphNamespace {
  export type fastifyInstance = fastify.FastifyInstance<http.Server, http.IncomingMessage, http.ServerResponse>
  export type ApiRouteGraphOptions = {
    instance: fastifyInstance
  }
}
