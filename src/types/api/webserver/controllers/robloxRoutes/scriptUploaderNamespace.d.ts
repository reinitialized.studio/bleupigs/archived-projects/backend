declare module scriptUploaderNamespace {
  export enum gql$$UploadRobloxAsset_assetTypeEnum {
    SANDBOX,
    CLIENT
  }

  export interface gql$$scriptUploadRequest {
    asset_name: string
    asset_contents: string
    asset_type: gql$$UploadRobloxAsset_assetTypeEnum
  }

  export interface gql$$responseData {
    asset_id: Number
    asset_version_id: Number
  }
}
