declare module robloxControllerNamespace {
  export interface dataLayer {
    serverCredentialsResponse?: serverValidation.serverCredentialResponse
    error?: utilValidationHelpers.UUIDError[]
  }

  export interface validateInstantiateRouteResponse {
    success: boolean,
    response: string,
    dataLayer?: dataLayer
  }

  export interface uploadRobloxAssetRequestParameters {
    requestData: scriptUploaderNamespace.gql$$scriptUploadRequest
  }
}
