declare module validateRobloxPlaceInstanceNamespace {
  import ActiveRobloxInstanceImpl = serverValidation_GraphQL.ActiveRobloxInstanceImpl;

  export interface validateRobloxPlaceInstanceRequestParameters {
    requestData: gql$$requestParameters
  }

  export interface refreshRobloxPlaceInstanceRequestParameters {
    requestData: gql$$refreshRequestParameters
  }

  export interface gql$$refreshRequestParameters {
    guild: string
    ip: string | string[]
  }

  export interface gql$$requestParameters {
    guild: string
    job_id: string
    ip: string | string[]
    place_id: string
  }

  interface authenticationNodesGraphQLResponse {
    alg: string
    token: string
    expiry: string | number;
    permission: string
  }

  interface authenticationNodes {
    graph_ql: authenticationNodesGraphQLResponse
    server_response?: ActiveRobloxInstanceImpl
  }

  export interface gql$$responseParameters {
    success: boolean
    authentication_nodes: authenticationNodes
  }
}


