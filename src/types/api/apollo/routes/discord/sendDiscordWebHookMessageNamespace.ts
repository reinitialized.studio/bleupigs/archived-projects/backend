declare module sendDiscordWebHookMessageNamespace {
  export interface gql$$requestData {
    user_name: string
    contents: string
    profile_picture: string
    timestamp: number
  }

  export interface gql$$responseData {
    sent: boolean
  }

  export interface uploadRobloxAssetRequestParameters {
    requestData: gql$$requestData
  }

  export interface webhookMap {
    id: string,
    timestamp: number
  }
}
