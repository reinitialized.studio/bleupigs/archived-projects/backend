declare module discordApiRoutesNamespace {
  export interface recentCachedMessagesRequest {
    since: number;
  }

  export interface messageQueue {
    author: String,
    nickname?: String,
    content: String,
    timestamp: number,
    hoist_or_highest_role_name?: String,
    hoist_or_highest_role_hex_color?: String,
    display_hex_color?: String
    is_webhook: boolean
    id: string
  }
}
