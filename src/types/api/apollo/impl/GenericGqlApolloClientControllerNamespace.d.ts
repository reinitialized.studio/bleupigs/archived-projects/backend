declare namespace GenericGqlApolloClientControllerNamespace {
  interface resolverFormat<T> {
    [key: string]: T
  }

  export interface getResolver {
    Query?: resolverFormat<Function>
    Mutations?: resolverFormat<Function>
  }
}
