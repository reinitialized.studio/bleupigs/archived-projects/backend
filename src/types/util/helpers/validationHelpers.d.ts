declare namespace utilValidationHelpers {
  interface validUUD {
    RegExp: RegExpMatchArray;
    length: number;
  }

  interface UUIDError {
     message: string;
  }

  export interface validateUUIDType {
    valid: boolean;
    data?: validUUD;
    error?: UUIDError;
  }

  export interface validateMultipleUUIDType {
    valid: boolean;
    data: validateUUIDType[];
  }

  type DefaultHeaders = { [k: string]: any }

  export interface shallowEqualsInterface {
    valid: boolean,
    stack: Array<boolean>
  }

  export interface isHierarchicallyValidatedInterface {
    isMet: boolean,
    requesterRole: config.validUserRole,
    requiredRole: config.validUserRole
  }
}
