declare namespace webServerDecorators {
  type Method = "DELETE"  | "HEAD" | "GET" | "PATCH" |
                "OPTIONS" | "PUT"  | "POST"
}
