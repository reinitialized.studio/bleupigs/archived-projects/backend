declare module serverValidation_GraphQL {
  export type Maybe<T> = T | null;
  /** All built-in and custom scalars, mapped to their actual values */
  export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
  };

  export interface ActiveRobloxInstance {
    __typename: string;
    running_servers: ActiveRobloxInstanceImpl[];
  }

  export interface InsertionRobloxInstance {
    __typename: string;
    insert_running_servers: {
      returning: ActiveRobloxInstanceImpl[]
    }
  }

  export interface DeletionRobloxInstance {
    __typename: string;
    delete_running_servers: {
      returning: ActiveRobloxInstanceImpl[]
    }
  }


  export type ActiveRobloxInstanceImpl = {
    __typename?: 'activeRobloxInstance';
    server_id?: Maybe<Scalars['Int']>;
    server_job_id?: Maybe<Scalars['String']>;
    server_guild?: Maybe<Scalars['String']>;
    server_ip?: Maybe<Scalars['String']>;
  };
}
