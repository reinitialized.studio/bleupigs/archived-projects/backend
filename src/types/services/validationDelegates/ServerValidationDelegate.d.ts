declare module serverValidation {

  export interface serverConnection {
    Address: string;
    Port: number;
  }

  export interface whiteListedAddresses {
    serverGuild?: string,
    machineAddress?: string | string[],
    serverJobId?: string
  }

  export interface whiteListedAddressesResponse {
    serverGuild: string;
    machineAddress: string;
    isValid: boolean;
  }

  export interface joinScriptRequestResponse {
    jobId: string,
    status: number,
    joinScriptUrl: string,
    authenticationUrl: string,
    authenticationTicket: string,
    message: string
  }

  export interface serverCredentialResponseRequest {
    success: boolean,
    response: string,
    data?: serverCredentialResponse | any
  }

  export interface serverCredentialResponse {
    ClientPort: number;
    MachineAddress: string;
    ServerPort: number;
    ServerConnections: serverConnection[];
    PingUrl: string;
    PingInterval: number;
    UserName: string;
    DisplayName: string;
    SeleniumTestMode: boolean;
    UserId: number;
    RobloxLocale: string;
    GameLocale: string;
    SuperSafeChat: boolean;
    CharacterAppearance: string;
    ClientTicket: string;
    NewClientTicket: string;
    GameId: string;
    PlaceId: number;
    MeasurementUrl: string;
    WaitingForCharacterGuid: string;
    BaseUrl: string;
    ChatStyle: string;
    VendorId: number;
    ScreenShotInfo: string;
    VideoInfo: string;
    CreatorId: number;
    CreatorTypeEnum: string;
    MembershipType: string;
    AccountAge: number;
    CookieStoreFirstTimePlayKey: string;
    CookieStoreFiveMinutePlayKey: string;
    CookieStoreEnabled: boolean;
    IsRobloxPlace: boolean;
    GenerateTeleportJoin: boolean;
    IsUnknownOrUnder13: boolean;
    GameChatType: string;
    SessionId: string;
    AnalyticsSessionId: string;
    DataCenterId: number;
    UniverseId: number;
    BrowserTrackerId: number;
    UsePortraitMode: boolean;
    FollowUserId: number;
    characterAppearanceId: number;
    CountryCode: string;
  }

  export interface requestHeaders {
    "User-Agent": string;
    Referer: string;
    Origin: string;
    Cookie: string;
  }
}
