declare module jwtFactory {
  type Algorithm =
    "HS256" | "HS384" | "HS512" |
    "RS256" | "RS384" | "RS512" |
    "ES256" | "ES384" | "ES512" |
    "PS256" | "PS384" | "PS512" |
    "none";

  // The expiry can either be a Unix time stamp or a string as <number><timeframe> notation.
  type Expiry = string | number;

  export interface signedJwt {
    key: string,
    expiry: Expiry
  }
}
