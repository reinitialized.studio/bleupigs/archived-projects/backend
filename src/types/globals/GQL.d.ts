declare namespace GQL {
  export interface GraphQLResponseRoot<T> {
    data?: T;
    errors?: Array<GraphQLResponseError>;
  }

  interface GraphQLResponseError {
    message: string;
    locations?: Array<GraphQLResponseErrorLocation>;
    [propName: string]: any;
  }

  interface GraphQLResponseErrorLocation {
    line: number;
    column: number;
  }

  interface GenericValidationGQLResponse<T> {
    success: boolean,
    response: T
  }
}
