import {DocumentNode} from "graphql";
import {GraphQLResolverMap} from "@apollographql/apollo-tools/src/schema/resolverMap";
import config from "src/config";

declare namespace ApolloServerLoaderNamespace {
  export interface GraphQLSchemaModule {
    typeDefs: DocumentNode;
    resolvers?: GraphQLResolverMap<any>;
  }

  export interface Context {
    userContext: userContext
    serverContext: serverContext
  }

  export interface userContext {
    role: config.validUserRole,
    isApplicantOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isGuestOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isMemberOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isEliteOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isResolverOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isArbitratorOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isRobloxOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface,
    isServerOrHigher?: utilValidationHelpers.isHierarchicallyValidatedInterface
  }

  export interface serverContext {
    forwardedHost: string | string[],
    forwardedFor: string | string[]
  }
}
