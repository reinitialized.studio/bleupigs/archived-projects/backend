import {ClientEvents} from "discord.js";

declare namespace DiscordClientNamespace {
  export interface requestMetadata {
    channel: Object;
    $$func?: Function
  }

  export class DiscordClientInterface {
    public bindEvent<K extends keyof ClientEvents>(event: K, requestMetadata, listener: (...args: ClientEvents[K]) => void): DiscordClientInterface;
  }
}
