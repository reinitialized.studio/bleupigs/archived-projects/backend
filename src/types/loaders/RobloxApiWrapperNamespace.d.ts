declare namespace RobloxApiWrapperNamespace {
  interface UploadModelResponse {
    AssetId: number;
    AssetVersionId : number;
  }

  interface uploadInstanceResponse {
    success: boolean,
    error?: string,
    response?: UploadModelResponse
  }
}
