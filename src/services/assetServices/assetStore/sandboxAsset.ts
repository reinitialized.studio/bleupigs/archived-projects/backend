export default (propertyMap: sandboxInterface.propertyMap) => {
  return `<roblox xmlns:xmime="http://www.w3.org/2005/05/xmlmime" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.roblox.com/roblox.xsd" version="4">
  <Meta name="ExplicitAutoJoints">true</Meta>
    <External>null</External>
    <External>nil</External>
    <Item class="ModuleScript" referent="RBX1CA8762626AA4ED9AEC8A1C1CD65F03B">
  <Properties>
    <BinaryString name="AttributesSerialize"></BinaryString>
    <Content name="LinkedSource"><null></null></Content>
  <string name="Name">MainModule</string>
    <string name="ScriptGuid">{BE345F1B-15DD-4CBB-AF1A-BDB347F20B80}</string>
    <ProtectedString name="Source"><![CDATA[local module = {}

  return module
]]></ProtectedString>
  <BinaryString name="Tags"></BinaryString>
    </Properties>
    <Item class="LocalScript" referent="RBX53425B468C264DF3BDBDE38F52DC9631">
  <Properties>
    <BinaryString name="AttributesSerialize"></BinaryString>
    <bool name="Disabled">false</bool>
    <Content name="LinkedSource"><null></null></Content>
  <string name="Name">LocalScript</string>
    <string name="ScriptGuid">{AA933A02-6159-4414-AE87-B6FF8373AAE5}</string>
    <ProtectedString name="Source"><![CDATA[${propertyMap.contents}]]></ProtectedString>
    <BinaryString name="Tags"></BinaryString>
    </Properties>
    </Item>
    </Item>
    </roblox>`
}
