import SANDBOX from "./assetStore/sandboxAsset"

/**
 * Utility class - provides XML resources from assetStore and allows clients to provide maps to
 * replace content within the XML files.
 */
export default class RobloxAssetHelper {
  private type: Function;
  private asset;
  private api;

  constructor(nobloxInstance) {
    this.api = nobloxInstance;
  }

  public setInstanceContents(assetContents): RobloxAssetHelper {
    this.asset = this.type(assetContents);
    return this;
  }

  public setType(assetStoreType: Function){
    this.type = assetStoreType;
    return this;
  }

  public async upload(assetName: string){
    if (!this.type || !this.asset){
      throw new Error("Either the type or asset is undefined. Ensure setType and setInstanceContents have been correctly called.")
    }

    return this.api.uploadModel(this.asset, {name: assetName});
  }
}

export const AssetStore = {
  SANDBOX: SANDBOX,
};

