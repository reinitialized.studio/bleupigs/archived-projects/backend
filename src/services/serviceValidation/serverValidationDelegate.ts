import request from "request-promise";
import GraphQLClient from "src/util/helpers/GraphQLClient";
import {assert} from "src/util/helpers/validationHelpers";

/**
 * Mocks a request to a Roblox server instance to request game information such as MachineAddress
 * by faking a join request.
 *
 * Original code written by Grilme99 on GitHub know as RoCheck - https://github.com/grilme99/RoCheck
 */
export default class ServerValidationDelegate {
  private readonly cookie: string;

  public constructor(cookie: string) {
    this.cookie = cookie;
  }

  public async requestServerCredentials(jobId: string, placeId: string): Promise<serverValidation.serverCredentialResponseRequest> {
    const joinScriptRequest = await request({
      uri: ServerValidationDelegate.generateLauncherUri(jobId, placeId),
      headers: this.createRequestHeaders(placeId),
      json: true,
    });

    if (joinScriptRequest.joinScriptUrl === null || joinScriptRequest.jobId === null){
      return {
        success: false,
        response: `Failed to validate instance. Error message: ${joinScriptRequest.message || "N/A"}`,
        data: joinScriptRequest
      }
    }

    const joinScriptResponse = await request({
      uri: joinScriptRequest.joinScriptUrl,
      json: true
    });

    return {
      success: true,
      response: "Extracted serverCredentialResponses'",
      data: JSON.parse(joinScriptResponse.toString().replace(/--.*\r\n/, ''))
    }
  }

  public async deleteInactiveInstance(authenticationMap: serverValidation.whiteListedAddresses):
    Promise<GQL.GenericValidationGQLResponse<serverValidation_GraphQL.DeletionRobloxInstance>>{
    assert(authenticationMap.serverGuild, "Server Guild cannot be null or empty!");

    let queryResponse = await new GraphQLClient()
      .setQuery(
        `mutation deleteInactiveRobloxInstance($instance_guild: uuid) {
          delete_roblox_server(where: {server_guild: {_eq: $instance_guild}}) {
            returning {
              server_id
            }
          }
        }`
      )
      .setVariables({
        instance_guild: authenticationMap.serverGuild,
      })
      .execute();

    return {
      success: Boolean(queryResponse.data),
      response: queryResponse.data || queryResponse.errors
    }
  }

  public async mapWhitelistMachineAddress(authenticationMap: serverValidation.whiteListedAddresses):
    Promise<GQL.GenericValidationGQLResponse<serverValidation_GraphQL.InsertionRobloxInstance>>{
    assert(authenticationMap.machineAddress, "Machine Address cannot be null or empty!");
    assert(authenticationMap.serverGuild, "Server Guild cannot be null or empty!");
    assert(authenticationMap.serverJobId, "Server JobId cannot be null or empty!");

    let queryResponse = await new GraphQLClient()
      .setQuery(
        `mutation setActiveRobloxInstance(
          $instance_guild: uuid,
          $server_ip: inet,
          $server_job_id: uuid
        ) {
          insert_roblox_server(
            objects: {
              server_guild: $instance_guild, 
              server_ip: $server_ip, 
              server_job_id: $server_job_id
            }
          ) {
            returning {
              server_id
              server_ip
              server_guild
            }
          }
        }`
      )
      .setVariables({
        instance_guild: authenticationMap.serverGuild,
        server_ip: authenticationMap.machineAddress,
        server_job_id: authenticationMap.serverJobId
      })
      .execute();

    return {
      success: Boolean(queryResponse.data),
      response: queryResponse.data || queryResponse.errors
    }
  }

  public async retrieveWhitelistedServers(authenticationMap: serverValidation.whiteListedAddresses): Promise<serverValidation.whiteListedAddressesResponse> {
    assert(authenticationMap.serverGuild, "Server Guild cannot be null or empty!");
    assert(authenticationMap.machineAddress, "MachineAddress cannot be null or empty!");

    let queryResponse: GQL.GraphQLResponseRoot<serverValidation_GraphQL.ActiveRobloxInstance> = await new GraphQLClient()
      .setQuery(
        `query getActiveRobloxInstance($server_ip: inet) {
          roblox_server(where: {server_ip: {_eq: $server_ip}}) {
            server_guild,
            server_ip,
          }
        }`
      )
      .setVariables({
        server_ip: authenticationMap.machineAddress,
      })
      .execute();

    return {
      serverGuild: queryResponse.data.running_servers[0]?.server_guild || null,
      machineAddress: queryResponse.data.running_servers[0]?.server_ip || null,
      isValid: queryResponse.data.running_servers[0]?.server_ip === authenticationMap.machineAddress &&
               queryResponse.data.running_servers[0]?.server_guild === authenticationMap.serverGuild
    };
  }

  /**
   * TODO(Z_V): Passing the placeId around isn't nice. It's however not known at module initialisation
   * this could be improved by either implementing a factory/builder interface.
   */
  private static generateLauncherUri(jobId: string, placeId: string): string {
    return `https://assetgame.roblox.com/Game/PlaceLauncher.ashx?request=RequestGameJob&placeId=${placeId}&gameId=${jobId}`;
  }

  private createRequestHeaders(placeId: string): serverValidation.requestHeaders {
    return {
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
      Referer: `https://www.roblox.com/games/${placeId}/`,
      Origin: "https://www.roblox.com",
      Cookie: `.ROBLOSECURITY=${this.cookie}; path=/; domain=.roblox.com;`
    }
  }
}
