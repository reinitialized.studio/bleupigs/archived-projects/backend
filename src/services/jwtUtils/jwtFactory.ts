import * as jwt from "jsonwebtoken";
import {IllegalArgumentException} from "src/util/builtIns/Exceptions";
import config from "src/config"

/**
 * Wrapper for Json Web Tokens generation.
 */
export default class JwtFactory {
  public static userTypes = <config.validUserRole[]>[...Object.keys(config.auth.userTypes)];
  public static algorithm: jwtFactory.Algorithm = config.jwtConfig.algorithm;
  private static privateKey: string | Buffer = config.jwtConfig.secretKey;

  public constructClaim(chosenRole: config.validUserRole){
    if (!JwtFactory.userTypes.includes(chosenRole)){
      throw new IllegalArgumentException(`${chosenRole} does not conform to valid userTypes`);
    }

    return JwtFactory.signJWT({
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": [chosenRole],
        "x-hasura-default-role": chosenRole
      }
    }, JwtFactory.computeExpiry(chosenRole))
  }

  private static signJWT(body: Object, expiry: jwtFactory.Expiry): jwtFactory.signedJwt {
    return {
      key: jwt.sign(body, JwtFactory.privateKey, {
        algorithm: JwtFactory.algorithm,
        expiresIn: expiry
      }),
      expiry: expiry
    }
  }

  private static computeExpiry(role: config.validUserRole): jwtFactory.Expiry {
    return config.jwtConfig.expiryOverride[role] || config.jwtConfig.defaultExpiry
  }
}
